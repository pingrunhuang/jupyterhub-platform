import pandas as pd
from .user import User, Mysql_User, Redis_User, HBase_User
import getpass
'''
we just provide a way to read data from data warehouse and transform it into data frame
'''
class DataWarehouse:


    def __init__(self):
        username = getpass.getuser()
        self.user = User(username)


    def get_table_from_mysql(self, database, table_name):

        user = Mysql_User(self.user.username)
        conn = user.connect(database)
        if conn.is_connected():
            cur = conn.cursor()
            query = 'select * from ' + table_name
            cur.execute(query)
            df = pd.DataFrame(data for data in cur)
            # getting the column name
            descriptions = [x[0] for x in cur.description]
            result_df = pd.DataFrame()
            index = 0
            # because the original df is [0,1,2,3,4,description1,description2,description3,description4]
            # we want to abandon the first 4 columns. It might be better solution so perhaps change it in the future.
            for description in descriptions:
                result_df[description] = df[index]
                index += 1
            cur.close()
            user.disconnect()
            return result_df
        user.disconnect()


    '''
    @:return a data frame with 2 columns. The first column is the key, the second column is the value
    '''
    def get_from_redis(self, database):

        user = Redis_User(self.user.username)
        conn = user.connect(database)
        # getting all the keys in redis that are stored in the specified database
        keys = [key for key in conn.keys('*')]
        # getting all the values according to the keys
        values = [conn.get(key).decode('utf-8') for key in keys]
        df = pd.DataFrame()
        df['key'] = [key.decode('utf-8') for key in keys]
        df['value'] = values
        user.disconnect()
        return df


    '''
    @:return return a list of data frame. Each represent a table inside the specified namespace
    '''
    def get_from_hbase(self, namespace, decode_type='utf-8'):

        user = HBase_User(self.user.username)
        conn = user.connect(namespace)
        tables = conn.tables()
        dfs = {}
        for table_name in tables:
            table = conn.table(table_name)
            df = pd.DataFrame()
            data = table.scan()
            # each entry in data is a combination of {key, {dict}}
            keys = []
            values = []
            for (key, values) in data:
                keys = [x for x in values]
                values = [y.decode(decode_type) for y in (values[x] for x in keys)]
            df['{}:key'.format(table_name.decode(decode_type))] = [x.decode(decode_type) for x in keys]
            df['{}:value'.format(table_name.decode(decode_type))] = values
            dfs[table_name.decode(decode_type)] = df
        return dfs


    def get_from_data_factory(self):

        pass


    def fetch_by_database(self, database):
        pass




