#/bin/sh

## this is deprecated since we are using happybase instead
## set up thrift server and generate hbase thrift api

ROOT_DIR=pwd

wget http://mirror.bit.edu.cn/apache/thrift/0.10.0/thrift-0.10.0.tar.gz

tar -xvz thrift-0.10.0.tar.gz

cd thrift-0.10.0

./configure && make && make install

echo "Compiling hbase thrift api into python package..."

cd $ROOT_DIR

wget https://mirrors.tuna.tsinghua.edu.cn/apache/hbase/stable/hbase-1.2.6-src.tar.gz

tar -xvz hbase-1.2.6-src.tar.gz

cd hbase-1.2.6/hbase-thrift/src/main/resources/org/apache/hadoop/hbase/thrift

thrift --gen py Hbase.thrift

mv gen-py/* $ROOT_DIR

rm -rf gen-py/

cd $ROOT_DIR

echo "install thrift python package..."

pip install thrift

