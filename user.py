from mysql import connector
import redis
import happybase

'''
Problems:
    every time when we call connect method, we have to provide the database name or namespace in hbase, 
    but we might want to get all the data based on the user name. With this user we can access all the data that the user and access

TODO   
   should first list all the database or namespace in the server
   then iterate through each one and get all the data that could be access by the user
'''


class User:

    def __init__(self, username):
        self.username = username

    def connect(self, database, port, hostname):
        pass

    def disconnect(self):
        pass

    def identify(self):
        pass



class Mysql_User(User):

    def __init__(self, username):
        super().__init__(username)
        self.conn = None

    def connect(self, database, port=3306, hostname='localhost'):
        self.conn = connector.connect(user=self.username, host=hostname, port=port,
                                 database=database)
        return self.conn

    def disconnect(self):
        if self.conn != None:
            self.conn.close()



class Redis_User(User):

    def connect(self, database, port=6379, hostname='localhost'):
        self.conn = redis.StrictRedis(host=hostname, port=port, db=database)
        if not self.conn == None:
            return self.conn




class HBase_User(User):
    '''
    @:param the database
    @:return a connection to hbase
    '''
    def connect(self, namespace, port=9090, hostname='localhost'):
        if namespace == 'default':
            self.conn = happybase.Connection(host=hostname, port=port)
        else:
            self.conn = happybase.Connection(host=hostname, port=port, table_prefix_separator=':', table_prefix=namespace)
        return self.conn


    def disconnect(self):
        if self.conn != None:
            self.conn.close()