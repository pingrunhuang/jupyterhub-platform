from multiprocessing import Pool
from mylogger import timer
import pandas as pd
import re
import pickle

decode_type='utf-8'
is_decoded=False

result = {'accountId': [],
          'date': [],
          'time': [],
          'productName': [],
          'host': [],
          'productCode': [],
          'customerName': [],
          }

def pickling(df):
    with open('query_history.pickle', 'wb') as file:
        pickle.dump(df, file)

def process_one_line(line):
    print(line)
    if line.startswith('\s'):
        global decode_type, is_decoded
        row = {}
        if not is_decoded:
            fields = line.decode(decode_type).split('||')
        else:
            fields = line.split('||')
        if fields[1] == 'info' and fields[2] == 'host':
            row['host'] = fields[3]
        elif fields[1] == 'info' and fields[2] == 'productCode':
            row['productCode'] = fields[3]
        elif fields[1] == 'request' and fields[2] == 'accountId':
            row['accountId'] = fields[3]
        elif fields[1] == 'request' and fields[2] == 'customerName':
            row['customerName'] = fields[3]
        elif fields[1] == 'response' and fields[2] == 'result':
            rowkey = fields[0]
            product_name = rowkey.split()[0]
            row['productName'] = product_name
            date_detail = rowkey.split()[1]
            row['date'] = date_detail[:8]
            row['time'] = date_detail[8:10]
            row = fullfill_empty_row(row)

            for key in result:
                result[key].append(row[key])


def fullfill_empty_row(row):
    for key in result:
        if not row.keys().__contains__(key):
            row[key] = 'nil'
    return row

@timer
def start_processing(file_path):
    pool = Pool()
    with open(file=file_path, mode='rb') as file:
        # for entry in [next(file) for _ in range(1000)]:
        pool.map(process_one_line, file, 4)
    df = pd.DataFrame(result)
    pickling(df)


if __name__ == '__main__':
    start_processing(file_path='full_query_history_20170712')
