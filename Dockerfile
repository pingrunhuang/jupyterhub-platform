FROM daocloud.io/library/ubuntu

MAINTAINER pingrunhuang@gmail.com

WORKDIR /home
# install anaconda
RUN \
    apt-get update && \
    apt-get install wget -y && \
    wget --no-check-certificate https://repo.continuum.io/archive/Anaconda3-4.4.0-Linux-x86_64.sh && \
    bash Anaconda3-4.4.0-Linux-x86_64.sh

# install jupyterhub
RUN apt-get install npm nodejs-legacy && \
    npm install -g configurable-http-proxy && \
    pip install jupyterhub

