#!/bin/sh
set -e
if [ -z $1 ]
then
    echo "please specify the path of the file to be process the sensitivity info..."
    exit 1
fi

before=$1

sed -i.bak '/request:certNo/d;/request:mobile/d' $1

exit 0