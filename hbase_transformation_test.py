import unittest
from hbase_transformation import *
import configparser

class Test(unittest.TestCase):
    def setUp(self):
        conf = configparser.RawConfigParser()
        conf.read('/home/frank/dev/touch_stone/ts.conf')
        self.local_file_path = conf.get('hbase','local_file_path')
        transform_from_hbase_local_file('/home/frank/dev/touch_stone/full_query_history_20170712')

    def test_to_sql(self):
        df = sum_query_per_user_per_hour('query_history.pickle')
        save_to_mysql(df)

    # def test_sum_query_per_user_per_hour(self):
    #     sum_query_per_user_per_hour('query_history.pickle')
    #
    # def test_process_one_entry(self):
    #     expected = {'host':'192.168.20.74'}
    #     result = process_one_entry(b'03CMB00002\x1e20161012170156761605308\x1etest10_CMB00002_c1a8e059CMB_template30720||info||host||192.168.20.74')
    #     self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()
