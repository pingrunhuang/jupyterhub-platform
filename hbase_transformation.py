from mylogger import timer
import pandas as pd
import logging
import pickle


'''
We should thread the read operation here
'''
@timer
def transform_from_hbase_local_file(file_path, decode_type='utf-8'):
    '''
    result will be
    ------------------------------------------------------------------------------------------
    accountId | date | time | productName | host | productCode | customerName | originalData
    ------------------------------------------------------------------------------------------
    :param file_dir: the path of the hbase local file
    :param decode_type: how to decode the file
    :return: a dataframe containing all the necessary data
    '''
    logging.basicConfig(filename='{}.log'.format(transform_from_hbase_local_file.__name__), level=logging.DEBUG)
    result = {'accountId': [],
              'date': [],
              'time': [],
              'productName': [],
              'host': [],
              'productCode': [],
              'customerName': [],
              }
    row = {}
    with open(file=file_path, mode='rb') as file:
        # for entry in [next(file) for _ in range(1000)]:
        for entry in file:
            # read and process line by line
            # add the current row's k/v to the row
            row.update(process_one_entry(entry))
            # hit the end of the query entry
            if entry.decode(decode_type).__contains__('values:mobileInnetPeriod'):
                row = fullfill_empty_row(row, result)
                for key in result:
                    result[key].append(row[key])
    df = pd.DataFrame(result)
    logging.debug(df.head(100))
    with open('query_history.pickle', 'wb') as file:
        pickle.dump(df, file)
    return df

# this function make sure each record has the same length
def fullfill_empty_row(row, result):

    for key in result:
        if not row.keys().__contains__(key):
            row[key] = 'nil'
    return row

def process_one_entry(line, decoded=False, decode_type='utf-8', delimiter=' '):
    '''
    :param line: the content of a row inside the hbase local file
    :param decoded: if the entry is already decoded or note
    :param decode_type: how you are going to decode the entry
    :return: a customized dictionary
    '''
    if not decoded:
        line = line.decode(decode_type)

    fields = line.strip(' ').split(delimiter)
    result = {}
    column = fields[1].strip(',')
    cf = column.split('=')[1].split(':')[0]
    cq = column.split('=')[1].split(':')[1]
    value = fields[3].split('=')[1]
    if cf == 'info' and cq == 'host':
        result['host'] = value
    elif cf == 'info' and cq == 'productCode':
        result['productCode'] = value
    elif cf == 'request' and cq == 'accountId':
        result['accountId'] = value
    elif cf == 'request' and cq == 'customerName':
        result['customerName'] = value
    elif cf == 'values' and cq == 'mobileInnetPeriod':
        rowkey = fields[0].split('\\x1E')
        print(rowkey)
        product_name = rowkey[0]
        result['productName'] = product_name
        date_detail = rowkey[1]
        result['date'] = date_detail[:8]
        result['time'] = date_detail[8:10]
    return result



def sum_query_per_user_per_hour(pickle_path):

    with open(pickle_path, 'rb') as file:
        df = pickle.load(file)
    counts_by_date_df = pd.DataFrame(df.groupby(['date', 'time', 'accountId']).size().rename('query_per_hour')).reset_index()
    return counts_by_date_df


def save_to_mysql(df):
    from sqlalchemy import create_engine
    import configparser
    conf = configparser.RawConfigParser()
    conf.read('ts.conf')
    username = conf.get('mysql', 'username')
    passwd = conf.get('mysql', 'passwd')
    host = conf.get('mysql', 'host')
    port = conf.get('mysql', 'port')
    db = conf.get('mysql', 'db')
    engine = create_engine('mysql+mysqlconnector://{}:{}@{}:{}/{}?charset=utf8'.format(username, passwd, host, port, db))
    df.to_sql('query_per_day_per_hour', engine, schema=db, if_exists='replace', index=True)


