import logging
from datetime import datetime
from functools import wraps

def timer(func):
    start = datetime.now()
    logging.basicConfig(filename="{}.log".format(func.__name__), level=logging.DEBUG)
    @wraps(func)
    def wrapper_func(*args, **kwargs):
        result = func(*args, **kwargs)
        end = datetime.now()
        logging.debug('{} ran with {} sec'.format(func.__name__, end-start))
        return result
    return wrapper_func

